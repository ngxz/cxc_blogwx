/* HiTypecho-微信小程序版Typecho
   使用教程：www.hiai.top
   有任何使用问题请联系作者邮箱 goodsking@163.com
*/
//获取应用实例
const app = getApp()
Page({
  data: {
    StatusBar: app.globalData.StatusBar,
    CustomBar: app.globalData.CustomBar,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
  },
  /**
 * 用户点击右上角分享
 */
  onShareAppMessage: function () {},
  //赞赏支持
  showSupport() {
    wx.previewImage({
      urls: ['https://img.yuanrb.com/img/wxpay600.jpg'],
      current: 'https://img.yuanrb.com/img/wxpay600.jpg' // 当前显示图片的http链接      
    })
  },
})
