var API = require('../../utils/api');
// 获取全局应用程序实例对象
var app = getApp();
var Net = require('../../utils/net');
function ranktype(name, active, idx) {
  this.name = name;
  this.active = active;
  this.idx = idx;
}
// 创建页面实例对象
Page({
  /**
   * 页面名称
   */
  name: "cat",
  /**
   * 页面的初始数据
   */
  data: {
    allcatslist: [],
    allcatpostlist: [],//所有文章
    catpostlist: [],
    ranklist: [],
    active_idx: 0,
    windowHeight: 100,
    current_cat_mid: -1,
    searchkeyword: '',
    allrankpostlist: [null, null, null],
    spec_arr:['计算机科学与技术']
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowHeight: res.windowHeight
        })
      }
    })
    this.setData({
      active_idx: 0,
    })
    // 默认展示排名文章
    // this.fetchrank(0);
    // 获取分类
    this.fetchallcats();
    // 获取热门
    // this.fetchpostbymid(); 
  },
    //文章的分类
    fetchallcats() {
      var that = this;
      Net.request({
        url: API.GetCat(),
        success: function (res) {
          var datas = res.data.data;
          datas.splice(0, 1);
          that.data.allcatslist = datas.map(function (item) {
            item.id_tag = "mid_" + item.mid;
            return item;
          });
          that.data.allcatpostlist = datas.map(function (item) {
            return null;
          });
          if (that.data.allcatslist.length > 0) {
            that.changeCatex(that.data.allcatslist[0].mid);
          }
          that.setData({
            allcatslist: that.data.allcatslist
          })
        }
      })
    },
    // 点击类别
    changeCat(e) {
      this.data.current_cat_mid = e.target.dataset.mid;
      var idx = this.getmidindex(this.data.current_cat_mid);
      if (idx != this.data.current_cat) {
        this.setData({
          current_cat: idx
        })
        this.changeCatex(this.data.current_cat_mid);
      }
    },
    getmidindex(mid) {
      for (var i = 0; i < this.data.allcatslist.length; i++)
        if (mid == this.data.allcatslist[i].mid) {
          return i;
        }
    },
    // 设置当前点击类别的样式
    changeCatex(mid) {
      this.setData({
        catpostlist: []
      })
      this.data.allcatslist = this.data.allcatslist.map(function (item) {
        if (item.mid == mid)
          item.active = true;
        else
          item.active = false;
        return item;
      })
      this.setData({
        allcatslist: this.data.allcatslist
      })
      // 获取当前点击类别的文章
      this.fetchpostbymid(mid);
    },
  //打印根据分类来打印的10篇文章
  fetchpostbymid(mid) {
    var that = this;
    var idx = this.getmidindex(mid);
    Net.request({
      url: API.GetPostsbyMID(mid),
      success: function (res) {
        var datas = res.data.data;
        // console.log(datas);
        if (datas != null && datas != undefined) {
          that.data.allcatpostlist[idx] = datas.map(function (item) {
            item.posttime = API.getcreatedtime(item.created);
            return item;
          });
          
          that.setData({
            allcatpostlist: datas,
            // allcatpostlist: that.data.allcatpostlist,
            postheight: that.data.allcatpostlist[idx].length * 170 + 'rpx'
          })
        } else {
          wx.showToast({
            title: '该分类没有文章',
            image: '../../images/error1.png',
            duration: 2000
          })
        }
      }
    })
  },
  fetchrank(idx) {
    var that = this;
    Net.request({
      url: API.GetRankedPosts(idx),
      success: function (res) {
        var datas = res.data.data;
        var rank = 1;

        that.data.allrankpostlist[idx] = datas.map(function (ori_item) {
          var item = API.ParseItem(ori_item);
          item.posttime = API.getcreatedtime(item.created);
          item.rank = rank++;//浏览量++
          return item;
        });
        that.setData({
          allrankpostlist: that.data.allrankpostlist,
          postheight: that.data.allrankpostlist[idx].length * 170 + 'rpx',
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.fetchallcats();
  },
})

